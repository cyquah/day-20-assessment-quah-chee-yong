(function () {
    angular
        .module("GroceryApp")
        .service("ShowService", ShowService);
    ShowService.$inject = ["$http", "$q"];
    function ShowService($http, $q) {
        var vm = this;
        
        vm.deleteProduct=function (Upc12, Id) {
            var defer = $q.defer();
            $http.delete("/api/products/" + Upc12 + "/" + Id)
                .then(function (response) {
                    // This is a good way to understand the type of results you're getting
                    defer.resolve(response);
                    // Calls search() in order to populate product info with predecessor of deleted product
                    search();
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > deleteProduct() > error: \n" + JSON.stringify(err));
                });
            return (defer.promise);
        };

        vm.save=function (Upc12,brandName) {
            var defer = $q.defer();
            $http.put("/api/brands/" + Upc12,
                {
                    upc12: Upc12,
                    brand: brandName
                })
                .then(function (response) {
                    defer.resolve(response);
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            return (defer.promise);
        };
        
        vm.search=function (Upc12) {
            var defer = $q.defer();
            $http.get("/api/brands/" + Upc12)
                .then(function (response) {
                    defer.resolve(response);
                })
                .catch(function (err) {
                   
                });
            return (defer.promise);
        }
    }
})();