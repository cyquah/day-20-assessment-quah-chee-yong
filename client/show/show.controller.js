(function () {
    'use strict';
    angular
        .module("GroceryApp")
        .controller("ShowCtrl", ShowCtrl);

    ShowCtrl.$inject = ["ShowService"];
    function ShowCtrl(ShowService) {
        console.log("-- in show.controller.js");
        // Exposed variables
        var vm = this;

        vm.upc12 = "";
        vm.result = {};

        // Exposed functions
        vm.deleteProduct = deleteProduct;
        vm.toggleEditor = toggleEditor;
        vm.initDetails = initDetails;
        vm.save = save;
        vm.search = search;

        // Initial calls
        initDetails();

        // Initializes product details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.upc12 = "";
            vm.result.brand = "";
            vm.result.id = "";
            vm.result.name = "";
            vm.showDetails = false;
            vm.isEditorOn = false;
        }

        // Function definition
        // Deletes displayed product. Details of preceding product is then displayed.
        function deleteProduct() {
            console.log("-- show.controller.js > deleteProduct()");
            ShowService.deleteProduct(vm.result.upc12,vm.result.id)
                .then(function (response) {
                    console.log("-- show.controller.js > deleteProduct() > res obj after delete req: \n" + response);
                })
                .catch((function (error) {
                }))
        }

        // Saves edited brand name
        function save() {
            console.log("-- show.controller.js > save()");
            ShowService.save(vm.upc12,vm.result.brand)
                .then(function (response) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(response.data));
                })
                .catch(function (error) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }

        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
            initDetails();
            vm.showDetails = true;
            ShowService.search(vm.upc12)
                .then(function (response) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(response.data));

                })
                .catch(function (error) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                })
        }

        // Switches editor state of the brand name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

    }
})();