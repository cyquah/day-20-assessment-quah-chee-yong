(function () {
    angular
        .module("GroceryApp")
        .config(GroceryAppConfig);
    GroceryAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

    function GroceryAppConfig($stateProvider,$urlRouterProvider){

        $stateProvider
            .state('B',{
                url : '/B',
                templateUrl :'./search/search.html',
                controller : 'SearchCtrl',
                controllerAs : 'ctrl'
            })
            .state('C',{
                url : '/C',
                templateUrl :'./show/show.html',
                controller : 'ShowCtrl',
                controllerAs : 'ctrl'
            })

        $urlRouterProvider.otherwise("/B");


    }
})();