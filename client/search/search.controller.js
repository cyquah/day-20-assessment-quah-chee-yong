(function () {
    angular
        .module("GroceryApp")
        .controller("SearchCtrl", SearchCtrl);

    SearchCtrl.$inject = ["$http", "$state", "SearchService"];

    function SearchCtrl($http, $state, SearchService) {
        var vm = this;

        vm.brandName = '';
        vm.result = null;
        vm.showProduct = false;

        // Exposed functions
        vm.search = search;
        vm.searchForProduct = searchForProduct;

        // Initial calls
        init();


        // Function definitions
        function init() {
            SearchService.initProductData()
                .then(function (response) {
                    vm.brands = response;
                })
                .catch(function (error) {
                    cosole.log("error " + error);
                })
        }

        function search() {
            vm.showProduct = false;
            SearchService.search( {
                params: {
                    'brand': vm.brandName
                }
            }).then(function (response) {
                vm.brands = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        function searchForProduct() {
            vm.showProduct = true;
            SearchService.search( {
                params: {
                    'brand': vm.brandName
                }
            }).then(function (response) {
                vm.brands = response;
            }).catch(function (error) {
                console.log(error);
            })
        }

        vm.showBrands = function (Upc12) {
            var UPC12 = Upc12;
        }
    }

})();