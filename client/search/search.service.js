(function () {
    angular
        .module("GroceryApp")
        .service("SearchService", SearchService);
    SearchService.$inject = ["$http", "$q"];
    function SearchService($http, $q) {
        var vm = this;

        vm.initProductData = function () {
            var defer = $q.defer();
            $http
                .get("/api/brands")
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.log("error " + err);
                });
            return (defer.promise);
        };

        vm.search = function (brandData) {
            var defer = $q.defer();
            $http
                .get("/api/brands",
                    brandData
                )
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
            return (defer.promise);
        };

        vm.searchProduct = function (brandData) {
            var defer = $q.defer();
            $http.get("/api/products",
                brandData
                )
                .then(function (response) {
                    defer.resolve(response.data);
                })
                .catch(function (err) {
                    console.info("Error " + err);
                });
            return (defer.promise);
        }
    }
})();