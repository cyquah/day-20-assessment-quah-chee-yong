//Load dependencies
var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

/* Serve files from public directory  __dirname is the absolute path of
 the application directory */

app.use(express.static(__dirname + "/../client/"));

//Create a connection with mysql
var MYSQL_USERNAME = 'root';
var MYSQL_PASSWORD = 'texaco789';
var conn = new Sequelize('grocery_list', MYSQL_USERNAME, MYSQL_PASSWORD, {});

var Brand = require('./models/brand')(conn, Sequelize);
var Product = require('./models/product')(conn, Sequelize);

// delete, update, and searchByID; also includes API naming;
// uses named parameters and req.body
// Searches for specific brand by upc12
app.get("/api/brands/:upc12", function (req, res) {
    console.log("-- GET /api/brands/:upc12");
    console.log("-- GET /api/brands/:upc12 request params: " + JSON.stringify(req.params));

    var where = {};
    if (req.params.Upc12) {
        where.Upc12 = req.params.Upc12
    }
    Brand
        .findAll({
            where: where
            , include: [{
                model: Product
                , order: [["ascending", "DESC"]]
                , limit: 20
            }]
        })

        .then(function (brands) {
            console.log("-- GET /api/brands/:upc12 findOne then() result \n " + JSON.stringify(brands));
            res.json(brands);
        })

        .catch(function (err) {
            console.log("-- GET /api/brands/:upc12 findOne catch() \n " + JSON.stringify(brands));
            res
                .status(500)
                .json({error: true});
        });
});
// -- Updates brand info
app.put("/api/brands/:upc12", function (req, res) {
    console.log("-- PUT /api/brands/:upc12");
    console.log("-- PUT /api/brands/:upc12 request params: " + JSON.stringify(req.params));
    console.log("-- PUT /api/brands/:upc12 request body: " + JSON.stringify(req.body));

    var where = {};
    where.upc12 = req.params.upc12;

    // Updates brand detail
    Brand
        .update(
            {brand: req.body.brands}             // assign new values
            , {where: where}                            // search condition / criteria
        )
        .then(function (response) {
            console.log("-- PUT /api/brands/:upc12 brand.update then(): \n"
                + JSON.stringify(response));
        })
        .catch(function (err) {
            console.log("-- PUT /api/brands/:upc12 brand.update catch(): \n"
                + JSON.stringify(err));
        });

});
// -- Searches for and deletes product of a specific brand.
app.delete("/api/products/:upc12", function (req, res) {
   console.log("-- DELETE /api/products/:upc12 request params: " + JSON.stringify(req.params));
    var where = {};
    where.upc12 = req.params.upc12;
    console.log("where " + JSON.stringify(where));

    Product
        .destroy({
            where: where
        })
        .then(function (result) {
            console.log("-- DELETE /api/products/:upc12 then(): \n" + JSON.stringify(result));
            if (result == "1")
                res.json({success: true});
            else
                res.json({success: false});

        })
        .catch(function (err) {
            console.log("-- DELETE /api/products/:upc12 catch(): \n" + JSON.stringify(err));
        });
});

    app.get("/api/brands", function (req, res) {
        var where = {};

        if (req.query.brand) {
            where.brand = {
                $like: "%" + req.query.brand + "%"
            }
        }

        if (req.query.upc12) {
            where.upc12 = req.query.upc12;
        }

        console.log("where: " + JSON.stringify(where));
        Brand
            .findAll({
                where: where
            })
            .then(function (brands) {
                res.json(brands);
            })
            .catch(function () {
                res
                    .status(500)
                    .json({error: true})
            });

    });
    app.get("/api/products", function (req, res) {
        var where = {};

        console.log("query: " + JSON.stringify(req.query));
        if (req.query.brand) {
            where.brand = {
                $like: "%" + req.query.brand + "%"
            }
        }
        Brand
            .findAll({
                where: where

                , include: [{
                    model: Product
                    , order: [["ascending", "DESC"]]
                    , limit: 20

                }]
            })

            .then(function (brands) {
                console.log("brands " + JSON.stringify(brands));
                res.json(brands);
            })

            .catch(function (err) {
                console.log("error " + err);
                res
                    .status(500)
                    .json({error: true});
            });
    });


//Start the web server on port 3000
    app.listen(3000, function () {
        console.info("Webserver started on port 3000");
    });