//Create a model for brands table
module.exports = function (conn, Sequelize) {
    var Brand = conn.define("brands", {
        upc12: {
            type: Sequelize.STRING,
            primaryKey: true
        },
        brand: {
            type: Sequelize.STRING
        }
    }, {
        timestamps: false
    });

    return Brand;
};