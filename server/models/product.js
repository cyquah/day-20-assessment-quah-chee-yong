//Create a model for product table
module.exports = function (conn, Sequelize) {
    var Product = conn.define('product', {
            upc12: {
                type: Sequelize.STRING,
                allowNull: false,
                primaryKey: true,
                references: {
                    model: 'brands',
                    key: 'upc12'
                }
            }

        }, {
            timestamps: false,
            tableName: "brand_product"
        }
    );

    return Product;
};